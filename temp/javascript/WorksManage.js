
// 側欄切換
function pageChange(type){
    var id="#rad-"+type;
    var title = document.getElementsByClassName("title")[0];

    document.getElementsByClassName("insert-btn")[0].style.display="none";
    document.getElementsByClassName("insert-btn")[1].style.display="none";
    var workbtn=document.getElementsByClassName("insert-work")[0];
    var companybtn=document.getElementsByClassName("insert-company")[0];

    if(type=="work"){
        title.innerHTML = "經歷管理";
        workbtn.style.display="flex";
    }else if(type=="company"){
        title.innerHTML = "公司管理";
        companybtn.style.display="flex";
    }
    document.querySelector(id).checked = true;
}

// 重整頁面帶值以切換側欄
window.onload=function (){
    var url=location.href;
    var str2='';
    companyCount = document.getElementsByClassName("company-edit").length;
    count = companyCount;

    if(url.indexOf('?')!=-1){
       var str=url.split('?');
       str2=str[1] 
    }
    if (str2=='') {
        str2='work';
    }
    pageChange(str2);
}

// 公司管理的欄位新增
function newCompany(){
    count += 1;
    var parentDiv = document.getElementsByClassName("companies")[0];
    var insertDiv = '<div class="company'+count+' company">'+
                    '<input class="company-edit" id="company'+count+'" type="text">'+
                    '<div class="del-btn" id="del'+count+'" onclick="delCompany('+count+')"><img src="icon/delete.png"></div>'+
                    '</div>';
    parentDiv.insertAdjacentHTML('beforeend', insertDiv);
}

// 刪除一筆公司欄位
function delCompany(id){
    var delId = '.company'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}
