function pagechange(type){
    var id="";
    var a = document.getElementsByClassName("topic-2")[0];
    document.getElementById("btn-c").style.display="none";
    document.getElementById("btn-s").style.display="none";
    document.getElementById("btn-p").style.display="none";
    document.getElementById("btn-e").style.display="none";
    if(type=="contest"){
        id="#radio-c";
        a.innerHTML = "競賽紀錄";
        var b= document.getElementById("btn-c");
        b.style.display="flex";
    }else if(type=="subject"){
        id="#radio-s";
        a.innerHTML = "專題紀錄";
        var b= document.getElementById("btn-s");
        b.style.display="flex";
    }
    else if(type=="project"){
        id="#radio-p";
        a.innerHTML = "研究計劃";
        var b= document.getElementById("btn-p");
        b.style.display="flex";
    }
    else if(type=="essay"){
        id="#radio-e";
        a.innerHTML = "碩士論文";
        var b= document.getElementById("btn-e");
        b.style.display="flex";
    }
    document.querySelector(id).checked = true;
}
window.onload=function (){
    var url=location.href;
    extraMemberCount = document.getElementsByClassName("chk-extraMembers").length;
    countM = extraMemberCount;
    filesCount = document.getElementsByClassName("chk-files").length;
    countF = filesCount;
    if(url.indexOf('?')!=-1){
       var str=url.split('?');
       var str2=str[1] 
    }else{
        var str2='contest';
    }
    pagechange(str2);
    
}

function countpeople(){
    var obj = document.getElementById("del");
    if(obj!=null){
        document.getElementById('del').outerHTML="";
    }

    var num=document.getElementById("member").value;
    var html2='<tr name="mem"><td>組員：<input class="inputbox3"  type="text"></td></tr>'
    document.querySelector('#members').insertAdjacentHTML('afterbegin','<tbody id="del"></tbody>');
    for(i=1;i<=num;i++){
        console.log("insert"+i);
        document.querySelector('#del').insertAdjacentHTML('afterbegin',html2);
    }  
}

// [新增、修改] 額外成員checkbox判斷
function extraMember(id) {
    var checkId = 'chk-exm'+id;
    var isChecked = document.getElementById(checkId).checked;
    if (isChecked) {
        newMember();
    }
    else {
        delMember(id);
    }
    // alert(isChecked);
}

// 額外成員的欄位新增
function newMember(){
    countM += 1;
    var parentTbody = document.getElementsByClassName("chk-extraMembers")[0];
    var insertTr = '<tr class="chk'+countM+'">'+
                   '<td><input type="checkbox"  class="check" id="chk-exm'+countM+'" onchange="extraMember('+countM+')"><label for="chk-exm'+countM+'">額外成員</label></td>'+
                   '<td><input class="inputbox3" type="text"></td>'+
                   '</tr>';
    parentTbody.insertAdjacentHTML('beforeend', insertTr);
}

// 刪除額外成員欄位
function delMember(id){
    var delId ='.chk'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}

// [新增、修改] 檔案checkbox判斷
function fileCheck(id) {
    var checkId = 'chk-file'+id;
    var isChecked = document.getElementById(checkId).checked;

    if (isChecked) {
        newFile();
    }
    else {
        var yes = confirm('你確定要刪除嗎？')
        if (yes){
            delFile(id);
        }
        else{
            document.getElementById(checkId).checked="true";
        }
    }
}

// 檔案的欄位新增
function newFile(){
    countF += 1;
    var parentTbody = document.getElementsByClassName("chk-files")[0];
    var insertTr = '<tr class="chk-file'+countF+'">'+
                    '<td>'+
                    '<input type="checkbox"  class="check" id="chk-file'+countF+'" onchange="fileCheck('+countF+')">'+
                    '<input class="upload" type="file">'+
                    '</td>'+
                    '</tr>';
    parentTbody.insertAdjacentHTML('beforeend', insertTr);
}

// 刪除檔案欄位
function delFile(id){
    var delId ='.chk-file'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}