var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/Honors/Honor?Id=2');
request.setRequestHeader("Content-Type","application/json")
request.send();

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        console.log(response);
        contests=response.C_DataList;
        members=response.P_DataList;
        console.log(contests);
        console.log(members);
    }else{
        console.log('error');
    }
};

window.onload=function(){
    for(i in contests){
        newworkcard(i);
    }
}

function newworkcard(i){
    var contest_id=contests[i].CompetitionId;
    var contest_name=contests[i].CompetitionName;
    var contest_type=contests[i].CompetitionType;
    var project_name=contests[i].Team.ProjectName;
    var contest_intro=contests[i].CompetitionIntroduct;
    var team="";
    for(x in members){
        var member=members[x].MemberData.Name.trim();
        team+=member+",";
    }
    var extra_member=contests[i].Team.ExtraMember.trim();
    team+=extra_member;
    console.log(team);

    var parentDiv=document.getElementsByClassName('content-s')[0];
    var insertDiv=
    '<div class="card1 color_transition" onclick="location.href=\'f_subject_detail.html?'+contest_id+'\'">'+
    '<table>'+
        '<tr>'+
            '<td class="td-title">參展名稱：</td><td class="td-con">'+contest_name+'</td>'+
            '<td class="td-title">專題組別：</td><td class="td-con">'+contest_type+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="td-title">專題組員：</td><td colspan="2" class="intro">'+team+'</td>'+
        '</tr>'+         
        '<tr>'+
            '<td colspan="4"><hr></td>'+
        '</tr>'+
        '<tr>'+
            '<td class="td-title">專題名稱：</td><td colspan="3" class="intro">'+project_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="td-title">專題介紹：</td>'+
            '<td class="intro" colspan="3">'+
                '<div class="intro-block">'+
                    contest_intro+
                '</div>'+
            '</td>'+
        '</tr>'+
    '</table>'+
'</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}