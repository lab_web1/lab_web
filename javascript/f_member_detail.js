var url=location.href;
var str=url.split('?');
var memberid=str[1];
console.log(memberid);

var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/MemberManagement/GetAMemberDatas?Id='+memberid);
request.setRequestHeader("Content-Type","application/json")

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        member=response;
        console.log(member);
    }else{
        console.log('error');
    }
};
request.send();

window.onload=function(){
    newworkcard();
    for(i in member){
        getskill(i);
    }
}
function getskill(i){
    var parentDiv=document.getElementsByClassName('sks')[0];
    var insertDiv=
    '<div class="sk">'+
        '<label>'+member[i].skills.SkillName;+'</label>'+
    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);
}
function newworkcard(){
    var membername=member[1].Name;
    var account=member[1].Member.Account;
    var sex=member[1].Gender;
    //照片

    var roleid=member[1].roles.RoleId;
    var role=member[1].roles.RoleName+" ";
    var session=member[1].Member.Session;
    var graduation=member[1].Member.IsGraduation;
    if(roleid==2){
        role+=session+".0";
    }
    else if(role==3){
        role+=session+"年"
    }
    if(graduation){
        role+='（畢業）'
    }
    var phone1=member[1].Phone.trim();
    if(phone1.length==0){
        var phone="待填"
    }else{
        var phone=member[1].Phone;
    }
    if(member[1].Email==""){
        var email="待填"
    }else{
        var email=member[1].Email;
    }

    var parentDiv=document.getElementsByClassName('setting')[0];
    var insertDiv=
    '<div class="card2">'+
    '<div class="pic">'+
        '<img src="icon/user.png">'+
    '</div> '+
    '<table>'+
        '<tr>'+
            '<td class="t-title">帳號：</td>'+
            '<td class="t-content">'+
                account+
            '</td>'+
            '<td class="t-title">身份：</td>'+
            '<td class="t-content">'+
               role+
            '</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="t-title">姓名：</td>'+
            '<td class="t-content">'+
                membername+
            '</td>'+
            '<td class="t-title">性別：</td>'+
            '<td class="t-content">'+
                sex+
            '</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="t-title">電話：</td>'+
            '<td class="t-content" colspan="3">'+phone+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="t-title">信箱：</td>'+
            '<td class="t-content" colspan="3">'+email+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="t-title">我的技能</td>'+
            '<td class="skills" colspan="3">'+
                '<div class="sks">'+
                    
                '</div>'+
            '</td>'+
        '</tr>'+
    '</table>'+
'</div>'+
'</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}