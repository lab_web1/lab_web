var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/Honors/Honor_Exhibitions?Id=3');
request.setRequestHeader("Content-Type","application/json")
request.send();

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        console.log(response);
        exhobotions=response.Exhobotion_DataList;
        members=response.P_DataList;
        console.log(exhobotions);
        console.log(members);
    }else{
        console.log('error');
    }
};

window.onload=function(){
    for(i in exhobotions){
        newworkcard(i);
    }
}

function newworkcard(i){
    var exhobotion_id=exhobotions[i].ExhibitionId;
    var exhobotion_name=exhobotions[i].ExhibitionName;
    var project_name=exhobotions[i].ProjectName;
    var project_intro=exhobotions[i].ExhibitionIntroduct;
    var team="";
    for(x in members){
        var member=members[x].MemberData.Name.trim();
        if(x!=members.length-1){
            team+=member+",";
        }else{
            team+=member
        }
        
    }
    console.log(team);

    var parentDiv=document.getElementsByClassName('content-e')[0];
    var insertDiv=
    '<div class="card1 color_transition" onclick="location.href=\'f_essay_detail.html?'+exhobotion_id+'\'">'+
    '<table>'+
        '<tr>'+
            '<td class="td-title">論文名稱：</td><td colspan="3" class="intro">'+project_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="td-title">論文類別：</td><td class="td-con" colspan="3">'+exhobotion_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="td-title">撰寫人員：</td><td colspan="2" class="intro">'+team+'</td>'+
        '</tr>'+         
        '<tr>'+
            '<td colspan="4"><hr></td>'+
        '</tr>'+
        
        '<tr>'+
            '<td class="td-title">論文介紹：</td>'+
            '<td class="intro" colspan="3">'+
                '<div class="intro-block">'+
                    project_intro+
                '</div>'+
            '</td>'+
        '</tr>'+
    '</table>'+
'</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}