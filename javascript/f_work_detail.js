var url=location.href;
var str=url.split('?');
var workid=str[1];
console.log(workid);

var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/WorkExperience/GetAWorkDatas?Id='+workid);
request.setRequestHeader("Content-Type","application/json")

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        work=response.work;
        console.log(work);
        work_data(work)
    }else{
        console.log('error');
    }
};
request.send();

function work_data(work){
    newworkcard(work);
}

function newworkcard(work){
    var membername=work.MemberData.Name;
    //照片

    var roleid=work.role.RoleId;
    var role=work.role.RoleName+" ";
    var session=work.Member.Session;
    var graduation=work.Member.IsGraduation;
    if(roleid==2){
        role+=session+".0";
    }
    else if(role==3){
        role+=session+"年"
    }
    if(graduation){
        role+='（畢業）'
    }
    var position=work.Position;
    var companyname=work.companyType.CompanyName;
    var Entry=work.EntryDate.substr(0,10).replaceAll('-','/');
    if(work.LeaveDate!=null){
        var Leave=works[i].LeaveDate.substr(0,10).replaceAll('-','/');
    }else {
        var Leave="迄今";
    }
    var Date=Entry+"-"+Leave;
    var workduty=work.WorkDuty;
    var feedback=work.Feedback;

    var parentDiv=document.getElementsByClassName('content')[0];
    var insertDiv=
    '<div class="personal">'+
    '<div class="personal-top">'+
        '<div class="person-image">'+
            '<img src="icon/male.jpg">'+
        '</div>'+
        '<div class="person-info">'+
            '<table>'+
                '<tr class="t-top">'+
                    '<td class="t-name" colspan="3">'+membername+' / <span>'+role+'</span></td>'+
                '</tr>'+
                '<tr>'+
                    '<td class="t-title">工作職稱：</td>'+
                    '<td class="t-content">'+position+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td class="t-title">公司名稱：</td>'+
                    '<td class="t-content">'+companyname+'</td>'+
                '</tr>'+
                '<tr>'+
                    
                    '<td class="t-title">就職日期：</td>'+
                    '<td class="t-content">'+Date+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td class="t-title">工作內容：</td>'+
                    '<td class="t-content big-content" colspan="3">'+workduty+'</td>'+
                '</tr>'+
            '</table>'+
        '</div>'+
    '</div>'+
    
    '<div class="feedback">'+
        '<table>'+
            '<tr>'+
                '<td colspan="4"><hr></td>'+
            '</tr>'+
            '<tr>'+
                '<td class="f-title">心得：</td>'+
                '<td class="f-content">'+feedback+'</td>'+
            '</tr>'+
        '</table>'+
    '</div>'+
'</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}