var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/WorkExperience/WorkDatas');
request.setRequestHeader("Content-Type","application/json")

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        console.log(response);
        works=response;
        console.log(works);
        works_datas(works)
    }else{
        console.log('error');
    }
};
request.send();

function works_datas(works){
    for(i in works){
        newworkcard(i);
    }
}

function newworkcard(id){
    var membername=works[i].MemberData.Name;
    //照片

    var workid=works[i].WorkId;
    var roleid=works[i].role.RoleId;
    var role=works[i].role.RoleName+" ";
    var session=works[i].Member.Session;
    var graduation=works[i].MemberIsGraduation;
    if(roleid==2){
        role+=session+".0";
    }
    else if(role==3){
        role+=session+"年"
    }
    if(graduation){
        role+='（畢業）'
    }
    var position=works[i].Position;
    var companyname=works[i].companyType.CompanyName;
    var Entry=works[i].EntryDate.substr(0,10).replaceAll('-','/');
    if(works[i].LeaveDate!=null){
        var Leave=works[i].LeaveDate.substr(0,10).replaceAll('-','/');
    }else {
        var Leave="迄今";
    }
    
    var Date=Entry+"-"+Leave;

    var parentDiv=document.getElementsByClassName('works')[0];
    var insertDiv=
    '<div class="work color_transition" onclick="location.href=\'f_work_detail.html?'+workid+'\'">'+
        '<div class="personal">'+
            '<div class="person-image">'+
                '<img src="icon/male.jpg">'+
            '</div>'+
            '<div class="person-info">'+
                '<table>'+
                    '<tr>'+
                        '<td colspan="2" class="t-main">'+membername+'/ <span>'+role+'</span></td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td class="t-title"><img src="icon/position.png"></td>'+
                        '<td class="t-content">'+position+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td class="t-title"><img src="icon/company.png"></td>'+
                        '<td class="t-content">'+companyname+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td class="t-title"><img src="icon/calendar.png"></td>'+
                        '<td class="t-content">'+Date+'</td>'+
                    '</tr>'+
                '</table>'+
            '</div>'+
        '</div>'+
    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}