var url=location.href;
var str=url.split('?');
var id=str[1];

var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/Honors/GetDataByExhibitions?Id='+id);
request.setRequestHeader("Content-Type","application/json")

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        console.log(response);
        exhobotion=response.Exhobotion_DataList;
        members=response.P_DataList;
        console.log(exhobotion);
        console.log(members);
    }else{
        console.log('error');
    }
};
request.send();

window.onload=function(){
    newworkcard();
}

function newworkcard(){
    var exhobotion_name=exhobotion[0].ExhibitionName;
    var project_name=exhobotion[0].ProjectName;
    var project_intro=exhobotion[0].ExhibitionIntroduct;
    var date=exhobotion[0].ExhibitionDate.substr(0,10).replaceAll('-','/');
    var team="";
    for(x in members){
        var member=members[x].MemberData.Name.trim();
        if(x!=members.length-1){
            team+=member+",";
        }else{
            team+=member
        }
        
    }
    console.log(team);

    var parentDiv=document.getElementsByClassName('center')[0];
    var insertDiv=
    '<table>'+
        '<tr>'+
            '<td class="w-title">研討會：</td><td class="w-content">'+exhobotion_name+'</td>'+
            '<td class="w-title">發表日期：</td><td class="w-content">'+date+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="w-title">論文名稱：</td><td class="w-content">'+project_name+'</td>'+
            '<td class="w-title">論文類別：</td><td class="w-content">'+exhobotion_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="w-title">研究人員：</td><td colspan="3" class="intro">'+team+'</td>'+
        '</tr>'+         
        '<tr>'+
            '<td class="w-title">論文介紹：</td>'+
            '<td class="w-content" colspan="3">'+
                project_intro+
            '</td>'+
        '</tr>'+
    '</table>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}