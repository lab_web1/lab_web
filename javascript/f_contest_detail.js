var url=location.href;
var str=url.split('?');
var id=str[1];

var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/Honors/GetDataByHonor?Id='+id);
request.setRequestHeader("Content-Type","application/json")

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        contest=response.C_DataList;
        members=response.P_DataList;
        console.log(contest);
        console.log(members);
    }else{
        console.log('error');
    }
};
request.send();

window.onload=function(){
    newworkcard();
}

function newworkcard(){
    var contest_name=contest[0].CompetitionName;
    var contest_type=contest[0].CompetitionType;
    var project_name=contest[0].Team.ProjectName;
    var contest_intro=contest[0].CompetitionIntroduct;
    var award=contest[0].Team.Award;
    var date=contest[0].CompetitionDate.substr(0,10).replaceAll('-','/');
    var team="";
    for(x in members){
        var member=members[x].MemberData.Name.trim();
        team+=member+",";
    }
    var extra_member=contest[0].Team.ExtraMember.trim();
    team+=extra_member;
    console.log(team);

    var parentDiv=document.getElementsByClassName('card2')[0];
    var insertDiv=
    '<table>'+
        '<tr>'+
            '<td class="w-title">競賽名稱：</td><td class="w-content">'+contest_name+'</td>'+
            '<td class="w-title">競賽日期：</td><td class="w-content">'+date+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="w-title">競賽組別：</td><td class="w-content">'+contest_type+'</td>'+
            '<td class="w-title">得獎名次：</td><td class="w-content">'+award+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="w-title">參賽人員：</td><td class="w-content" colspan="3">'+team+'</td>'+
        '</tr>'+                     
        '<tr>'+
            '<td class="w-title">作品名稱：</td><td class="w-content" colspan="3">'+project_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td class="w-title">作品介紹：</td>'+
            '<td class="w-content" colspan="3">'+
                contest_intro+
            '</td>'+
        '</tr>'+
    '</table>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}