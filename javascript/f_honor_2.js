// var script = document.createElement('script');
// 	script.setAttribute('type','application/javascript');
// 	script.setAttribute('src','./javascript/chart.js/package/dist/chart.js');	
//     document.getElementsByTagName('head')[0].appendChild(script);
var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/Honors/Statistics');
request.setRequestHeader("Content-Type","application/json")
request.send();
request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        var chart_datas=response;
        console.log(chart_datas);
        run_charts(chart_datas);
    }else{
        console.log('error');
    }
};


function run_charts(chart_datas) {
    newcard1(chart_datas);
    newcard2(chart_datas);
    contest_pie(chart_datas);
    subject_pie(chart_datas);
    chart_contest_subject(chart_datas);
    study_pie(chart_datas);
    chart_study(chart_datas);
};

function newcard1(chart_datas){
    var parentDiv=document.getElementsByClassName('contest_count')[0];
    var insertDiv=
    '<div class="s-card">'+
        '<img src="icon/prize3.png">'+
        '<div class="s-card-text">'+
            '累計競賽得獎'+
            '<text class="s-card-num">'+
                chart_datas.CompetitionAwardCount+
            '</text>'+
            '件'+
        '</div>'+
    '</div>'+
    '<div class="s-card">'+
        '<img src="icon/prize4.png">'+
        '<div class="s-card-text">'+
            '累計專題得獎'+
            '<text class="s-card-num">'+
                chart_datas.SubjectAwardCount+
            '</text>'+
            '件'+
        '</div>'+
    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);
}

function newcard2(chart_datas){
    var parentDiv=document.getElementsByClassName('study_count')[0];
    var insertDiv=
    '<div class="s-card">'+
        ' <img src="icon/study.png">'+
        '<div class="s-card-text">'+
            '累計發表學術研究'+
            '<text class="s-card-num">'+
                chart_datas.ExhibitionCount+
            '</text>'+
            '件'+
        '</div>'+
    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);
}

function contest_pie(chart_datas){
    var num1=chart_datas.CompetitionIsAwardProrpotion;
    var num2=chart_datas.CompetitionNotAwardProrpotion;
    var pei1 = echarts.init(document.getElementById('pei-contest'));
    var pei1_option = {
        title: {
            text: '競賽得獎比例',
            left: 'center',
            top: 20,
        },
        tooltip: {
            trigger: 'item'
        },
        series: [
            {
                top:30,
                type: 'pie',
                radius: '70%',
                center: ['50%', '50%'],
                data: [
                    {value: num1, name: '得獎'},
                    {value: num2, name: '未得獎'}
                ],
                label: {
                    color: '#0A1C2F'
                },
                labelLine: {
                    lineStyle: {
                        color: '#0A1C2F'
                    },
                    smooth: 0.2,
                    length: 10,
                    length2: 20
                },
                itemStyle: {
                    shadowBlur: 500,
                    shadowColor: 'rgba(0, 0, 0, 0.2)'
                },
                color: ['#f7da36','#B5C334'],
                animationType: 'scale',
                animationEasing: 'elasticOut',
                animationDelay: function (idx) {
                    return Math.random() * 200;
                }
            }
        ]
    };
    pei1.setOption(pei1_option);
}

function subject_pie(chart_datas){
    var num1=chart_datas.SubjectIsAwardProrpotion;
    var num2=chart_datas.SubjectNotAwardProrpotion;
    var pei2 = echarts.init(document.getElementById('pei-subject'));
    var pei2_option = {
        title: {
            text: '專題得獎比例',
            left: 'center',
            top: 20,
        },
        tooltip: {
            trigger: 'item'
        },
        series: [
            {
                top:30,
                type: 'pie',
                radius: '70%',
                center: ['50%', '50%'],
                data: [
                    {value: num1, name: '得獎'},
                    {value: num2, name: '未得獎'}
                ],
                label: {
                    color: '#0A1C2F'
                },
                labelLine: {
                    lineStyle: {
                        color: '#0A1C2F'
                    },
                    smooth: 0.2,
                    length: 10,
                    length2: 20
                },
                itemStyle: {
                    shadowBlur: 500,
                    shadowColor: 'rgba(0, 0, 0, 0.2)'
                },
                color: ['#f7da36','#E87C25'],
                animationType: 'scale',
                animationEasing: 'elasticOut',
                animationDelay: function (idx) {
                    return Math.random() * 200;
                }
            }
        ]
    };
    pei2.setOption(pei2_option);
}

function chart_contest_subject(chart_datas){
    var years=[];
    var contest_num=[];
    var subject_num=[];
    for(i in chart_datas.CompetitionNineYears){
        years[i]=chart_datas.CompetitionNineYears[i].year;
    }
    years.reverse();
    console.log(years);

    for(i in chart_datas.CompetitionNineYears){
        contest_num[i]=chart_datas.CompetitionNineYears[i].CompetitionNineYear;
    }
    contest_num.reverse();
    console.log(contest_num);

    for(i in chart_datas.CompetitionNineYears){
        subject_num[i]=chart_datas.CompetitionNineYears[i].SubjectNineYear;
    }
    subject_num.reverse();
    console.log(subject_num);


    var myChart = echarts.init(document.getElementById('main'));
    // 指定图表的配置项和数据
    var option = {
        title: {
            text: '歷年競賽得獎'
        }, 
        grid:{
            width:830,
            height:260
        },
        //提示框
        tooltip: {},
        //图例
        legend: {
            data:['競賽','專題']
        },
        //x軸座標
        xAxis: {
            color:'whitesmoke',
            name:'年份',
            data: years
        },
        yAxis: {
            color:'whitesmoke',
            name:'數量',
            nameTextStyle:{
                align:'right'
            },
            minInterval: 1
        },
        series: [{
            color:'#FFD900',
            name: '競賽',
            type: 'line',
            data: contest_num
        },
        {
            color:'#91cc75',
            name: '專題',
            type: 'line',
            data: subject_num
        }]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}

function study_pie(chart_datas){
    var num1=chart_datas.ExhibitionSeminarProrpotion;
    var num2=chart_datas.ExhibitionResearchProjectProrpotion;
    var num3=chart_datas.ExhibitionMastersThesisProrpotion;
    var pei3 = echarts.init(document.getElementById('pei-study'));
    var pei3_option = {
        title: {
            text: '學術研究發表類型',
            left: 'center',
            top: 20,
        },
        tooltip: {
            trigger: 'item'
        },
        series: [
            {
                top:30,
                type: 'pie',
                radius: '70%',
                center: ['50%', '50%'],
                data: [
                    {value: num1, name: '研討會'},
                    {value: num2, name: '研究計畫'},
                    {value: num3, name: '碩士論文'}
                ],
                label: {
                    color: '#0A1C2F'
                },
                labelLine: {
                    lineStyle: {
                        color: '#0A1C2F'
                    },
                    smooth: 0.2,
                    length: 10,
                    length2: 20
                },
                itemStyle: {
                    shadowBlur: 500,
                    shadowColor: 'rgba(0, 0, 0, 0.2)'
                },
                color: ['#fac858','#91cc75','#5470c6'],
                animationType: 'scale',
                animationEasing: 'elasticOut',
                animationDelay: function (idx) {
                    return Math.random() * 200;
                }
            }
        ]
    };
    pei3.setOption(pei3_option);
}

function chart_study(chart_datas){
    var years=[];
    var essay_num=[];
    var project_num=[];
    var seminar_num=[];
    for(i in chart_datas.ExhibitionNineYears){
        years[i]=chart_datas.ExhibitionNineYears[i].year;
    }
    years.reverse();
    console.log(years);

    for(i in chart_datas.ExhibitionNineYears){
        essay_num[i]=chart_datas.ExhibitionNineYears[i].ExhibitionMastersThesis;
    }
    essay_num.reverse();
    console.log(essay_num);

    for(i in chart_datas.ExhibitionNineYears){
        project_num[i]=chart_datas.ExhibitionNineYears[i].ExhibitionResearchProject;
    }
    project_num.reverse();
    console.log(project_num);

    for(i in chart_datas.ExhibitionNineYears){
        seminar_num[i]=chart_datas.ExhibitionNineYears[i].ExhibitionSeminar;
    }
    seminar_num.reverse();
    console.log(seminar_num);

    var myChart2 = echarts.init(document.getElementById('main2'));
    // 指定图表的配置项和数据
    var option2 = {
        title: {
            top:20,
            text: '歷年學術研究'
        }, 
        grid:{
            top:90,
            width:575,
            height:260
        },
        //提示框
        tooltip: {},
        //图例
        legend: {
            top:20,
            data:['研討會','研究計畫','碩士論文']
        },
        //x軸座標
        xAxis: {
            color:'whitesmoke',
            name:'年份',
            data: years
        },
        yAxis: {
            color:'whitesmoke',
            name:'數量',
            nameTextStyle:{
                align:'right'
            },
            minInterval: 1
        },
        series: [{
            color:'#FFD900',
            name: '研討會',
            type: 'bar',
            data: seminar_num
        },
        {
            color:'#91cc75',
            name: '研究計畫',
            type: 'bar',
            data: project_num
        },
        {
            color:'#5470c6',
            name: '碩士論文',
            type: 'bar',
            data: essay_num
        }]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart2.setOption(option2);
}