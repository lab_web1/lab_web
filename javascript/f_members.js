var request = new XMLHttpRequest();
request.open('GET', 'https://localhost:44308/api/MemberManagement/MemberDatas');
request.setRequestHeader("Content-Type","application/json")

request.onload = function() {
    if(this.status >= 200 && this.status < 400) {
        response=JSON.parse(request.response);
        console.log(response);
        members=response.MemberDataList;
        console.log(members);
    }else{
        console.log('error');
    }
};
request.send();

window.onload=function(){
    for(i in members){
        newworkcard(i);
    }
}

function newworkcard(id){
    var id=members[i].MemberDatasId;
    var membername=members[i].Name;
    var account=members[i].Member.Account;
    //照片

    var roleid=members[i].roles.RoleId;
    var role=members[i].roles.RoleName+" ";
    var session=members[i].Member.Session;
    var graduation=members[i].Member.IsGraduation;
    if(roleid==2){
        role+=session+".0";
    }
    else if(role==3){
        role+=session+"年"
    }
    if(graduation){
        role+='（畢業）'
    }
    var phone1=members[i].Phone.trim;
    if(phone1.length==0){
        var phone="待填"
    }else{
        var phone=members[i].Phone;
    }
    if(members[i].Email==""){
        var email="待填"
    }else{
        var email=members[i].Email;
    }
    

    var parentDiv=document.getElementsByClassName('members')[0];
    var insertDiv=
    '<div class="member" onclick="location.href=\'MemberDetail.html?'+id+'\'">'+
    '<div class="personal">'+
        '<div class="person-image">'+
            '<img src="icon/male.jpg">'+
        '</div>'+
        '<div class="person-info">'+
            '<table>'+
                '<tr>'+
                    '<td class="t-name">'+membername+'/ <span>'+role+'</span></td>'+
                '</tr>'+
                '<tr>'+
                    '<td class="t-content"><img src="icon/account.png">'+account+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td class="t-content"><img src="icon/phone.png">'+phone+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td class="t-content"><img src="icon/email.png">'+email+'</td>'+
                '</tr>'+
            '</table>'+
        '</div>'+
    '</div>'+
'</div>';
    parentDiv.insertAdjacentHTML('afterbegin',insertDiv);

}